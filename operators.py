# SPDX-License-Identifier: GPL-2.0-or-later

from typing import Iterable

import bpy
from mathutils import Vector


def snap_to_bone(object: bpy.types.Object, depsgraph: bpy.types.Depsgraph) -> None:
    """Snap the object to the nearest bone."""

    ob_pos = object.matrix_world.translation

    def distance_sq(bone_pos: Vector) -> float:
        # mypy doesn't know `.length_squared` returns a float.
        return (bone_pos - ob_pos).length_squared  # type: ignore

    closest_bone_pos = min(all_armature_bone_positions(depsgraph), key=distance_sq)
    object.matrix_world.translation = closest_bone_pos


def all_armature_bone_positions(depsgraph: bpy.types.Depsgraph) -> Iterable[Vector]:
    """Generator, yields all bone positions in world space."""

    scene = depsgraph.scene
    for scene_ob in scene.objects:
        if scene_ob.type != "ARMATURE":
            continue
        eval_scene_ob = scene_ob.evaluated_get(depsgraph)
        yield from armature_bone_positions(eval_scene_ob)


def armature_bone_positions(arm_ob: bpy.types.Object) -> Iterable[Vector]:
    """Generator, yields bone positions in world space."""

    assert arm_ob.type == "ARMATURE", f"expected ARMATURE, got {arm_ob.type}"

    wmat = arm_ob.matrix_world
    for pose_bone in arm_ob.pose.bones.values():
        yield wmat @ pose_bone.head
        yield wmat @ pose_bone.tail


class RigNodes_OT_snap_to_bone(bpy.types.Operator):
    """Snap a controller to the nearest bone"""

    bl_idname = "rignodes.snap_to_bone"
    bl_label = "Snap to Bone"
    bl_options = {"REGISTER", "UNDO"}

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return bool(context.object)

    def execute(self, context: bpy.types.Context) -> set[str]:
        snap_to_bone(context.object, context.view_layer.depsgraph)
        return {"FINISHED"}


classes = (RigNodes_OT_snap_to_bone,)
register, unregister = bpy.utils.register_classes_factory(classes)
